# elvui_updater

Python script that will download, unarchive, and install the latest elvui into your wow>_retail_>interface>addons folder.

I am using BeautifulSoup4 to programatically grab the update number the latest update number.

This is for use on MacOS but due to the similarities in MacOS and Windows versions of World of Warcraft the setup is similar.

[ElvUI](https://www.tukui.org/download.php?ui=elvui) is " A USER INTERFACE DESIGNED AROUND USER-FRIENDLINESS WITH EXTRA FEATURES THAT ARE NOT INCLUDED IN THE STANDARD UI. " for World of Warcraft.

## How to use the app

The following command will run the app.

```bash
    python update.py
```

Be sure to install your dependancies first. I tend to use [pipenv](https://docs.pipenv.org/en/latest/) for this work.

The following are valid arguments:

- -h --help = Displays help info
- -w --wow_path = Path of your base WOW install i.e. `/Applications/World of Warcraft/` arg defaults to that.

## ToDo

- display CHANGELOG after update
- additional error handling
