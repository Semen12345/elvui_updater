#!/usr/bin/python3
import sys
import urllib.request
import shutil
from zipfile import ZipFile
import getopt
import os
from bs4 import BeautifulSoup

# use BeauitfulSoup4 to grab version number
def get_update_version():
    
    # ELVui download page to scrape version from
    url = "https://www.tukui.org/download.php?ui=elvui"
    
    # build out the content request
    content = urllib.request.urlopen(url).read()

    # get your soup on
    soup = BeautifulSoup(content, features="html.parser")

    # capture version number and return as def response
    response = soup.find_all(class_='Premium')[0].get_text()
    return response

# download the elvui update file using get_update_version response
def get_update(update_version):

	update_url = 'https://www.tukui.org/downloads/elvui-' + update_version + '.zip'
	save_path = 'elvui_update.zip'
	
	try:
		with urllib.request.urlopen(update_url) as response, open(save_path, 'wb') as out_file:
			shutil.copyfileobj(response, out_file)
	except urllib.error.URLError as e:
		print(f"Update Version {update_version} {e.reason}")
		sys.exit(1)
		#usage()

	print(f"Download of {update_url} completed.")

def install_update(wow_path):
	install_path = wow_path + '_retail_/Interface/AddOns/'

	with ZipFile('elvui_update.zip', 'r') as zipObj:
		zipObj.extractall(install_path)
		print("Extraction over old add-on completed.")

def remove_download():
    os.remove('elvui_update.zip')
    print("ElvUI update file removed.")

# Define the usage of the app
def usage():
    """Prints usage information"""
    print()
    print("ElvUI Updater by prasket")
    print(" ")
    print("Usage: python update.py")
    print(" ")
    print("-h --help               	- this message")
    print("-w --wow_path			- *OPTIONAL* path to your base WOW install")
    print("                                    Default = /Applications/World of Warcraft/")
    print()
    print("Examples:")
    print("  Running with default install path:")
    print("    python update.py")
    print("  Running with custom install path:")
    print("    python update.py --wow_path /apps/wow")
    print()
    sys.exit(0)

def main():
    # set default wow path making -w argv optional
    wow_path = '/Application/World of Warcraft' # default wow install dir on MacOS. 

    try:
        opts, args = getopt.getopt(sys.argv[1:], "h:w:", ["help", "wow_path"])

    except getopt.GetoptError as err:
        print(str(err))
        usage()

    for o, a in opts:
        if o in ("-h", "--help"):
            usage()
        elif o in ("-w", "--wow_path"):
	        wow_path = a
        else:
            assert False, "Unhandled Option"

    # grab update version # 
    update_version = get_update_version()
    get_update(update_version)
    install_update(wow_path)
    remove_download()

    print(f"ElvUI has been updated to {update_version}")

if __name__ == "__main__":
    main()